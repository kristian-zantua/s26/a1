//5
let http = require(`http`);

//6 & 7
http.createServer((request, response) => {
//9
    if(request.url === "/login") {
        response.writeHead(200, {"Content-Type":"text/plain"});
        response.write("Welcome to the login page");
        response.end()
//10        
    } else {
        response.writeHead(404, {"Content-Type":"text/plain"});
        response.write("I'm sorry the page you are looking for cannot be found");
        response.end()
    }
}).listen(4000)